"""
把檔案拉進去後，再點summarization
"""
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from functions.dash_related import save_file, uploaded_files, file_download_link

import pandas as pd
import os
import functions.textrank as ft

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
DIRECTORY = "./docs"

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div([
    dcc.Upload(
        id='upload-data',
        children=html.Div([
            'Drag and Drop or ',
            html.A('Select Files')
        ]),
        style={
            'width': '100%',
            'height': '60px',
            'lineHeight': '60px',
            'borderWidth': '1px',
            'borderStyle': 'dashed',
            'borderRadius': '5px',
            'textAlign': 'center',
            'margin': '10px'
        },
        # Allow multiple files to be uploaded
        multiple=True
    ),
    html.Div(id='output-data-upload'),
    html.Div(
        [html.Button('Summarize', id='button'),
        html.Div(id='summarized_text')],
        ),
])

# for file uploading
@app.callback(Output('output-data-upload', "children"),
              [Input("upload-data", "contents")])
def update_output(uploaded_file_contents):
    """Save uploaded files and regenerate the file list."""
    if uploaded_file_contents is not None:
        for data in uploaded_file_contents:
            save_file("file_to_summarize", data)

    files = uploaded_files()
    if len(files) == 0:
        return [html.Li("No files yet!")]
    else:
        return [html.Li("File uploaded")]

# for summarize button
@app.callback(
    Output('summarized_text', 'children'),
    [Input('button', 'n_clicks')]
)
def update_output_div(n_clicks):

    if n_clicks != 0:
        name = "file_to_summarize"
        with open(os.path.join(DIRECTORY, name), "rb") as fp:
            file_to_summarize = fp.readlines()
        textrank = ft.TextRank(file_to_summarize, 0.2)
        textrank.input_preprocessing()
        textrank.build_similarity_matrix()
        textrank.pagerank()
        textrank.acquire_top_sentence()

    # save_file("file_summarized", textrank.Top_Sentence)

    return textrank.Top_Sentence


if __name__ == '__main__':
    app.run_server(debug=True)
