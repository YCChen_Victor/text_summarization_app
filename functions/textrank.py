from math import sqrt
import numpy as np
import networkx as nx
from nltk.corpus import stopwords
import nltk
nltk.download('punkt')
import string
import pandas as pd
from scipy.spatial.distance import euclidean, pdist, squareform


def square_rooted(x):
    return round(sqrt(sum([a * a for a in x])), 3)


def cosine_similarity(vector1, vector2):
    """
    :param vector1: vector of sentence one
    :param vector2: vector of sentence two
    :return: closeness between two vector
    """
    # the dot
    numerator = sum(a * b for a, b in zip(vector1, vector2))

    # the distance
    denominator = square_rooted(vector1) * square_rooted(vector2)

    return round(numerator / float(denominator), 3)

def similarity_func(u, v):
    return 1/(1+euclidean(u,v))

class TextRank:

    def __init__(self, txt, percent):
        # txt file
        self.txt = txt
        # splitted sentences
        self.Sentences = None
        # similarity matrix
        self.S_Matrix = None
        # PageRank values
        self.PageRank = None
        # the percentage of sentences to be acquried
        self.percent = percent
        # Top X% sentences
        self.Top_Sentence = None

    def input_preprocessing(self):

        # connect all string into one string
        full_string = ''
        for string in self.txt:
            string = string.decode() # 這邊要加入可以換編碼的
            full_string = full_string + string

        # split string into sentences
        sentences = nltk.tokenize.sent_tokenize(full_string)

        # clean the \n in sentences
        sentences_result = []
        for sentence in sentences:
            sentence = " ".join(sentence.split("\n"))
            sentences_result.append(sentence)

        self.Sentences = sentences_result

    def build_similarity_matrix(self, stop_words=stopwords):

        sentences = self.Sentences
        print(len(sentences))

        dict_vectorize_sentence = {}
        i = 0
        for sentence in sentences:
            sentence_list = sentence.split()
            words = [w.lower() for w in sentence_list]

            # remove punctuation from each word
            table = str.maketrans('', '', string.punctuation)
            stripped = [w.translate(table) for w in words]

            vectorize_sentence = {}
            for word in stripped:
                if word in stop_words.words():
                    continue
                else:
                    vectorize_sentence[word] = 1
            print(i)
            name = str(i)
            dict_vectorize_sentence[name] = vectorize_sentence
            i += 1

        df_data = pd.DataFrame(dict_vectorize_sentence)
        df_data = df_data.transpose()
        df_data = df_data.fillna(0)

        df_data.index = [("g" + str(i)) for i in range(1,(len(df_data.index)+1))]

        dists = pdist(df_data, similarity_func)
        DF_euclid = pd.DataFrame(squareform(dists), columns=df_data.index, index=df_data.index)
        self.S_Matrix = DF_euclid

    def pagerank(self):

        s_matrix = self.S_Matrix
        s_matrix = s_matrix.values
        g = nx.from_numpy_matrix(s_matrix, parallel_edges=True)
        pr_value = nx.pagerank(g, alpha=0.85)
        self.PageRank = pr_value

    @classmethod
    def acquire_top_index(cls, index_dict, X):

        sorted_by_value = sorted(index_dict.items(), key=lambda kv: kv[1],
                                 reverse=True)
        length = round(len(sorted_by_value) * X)
        choosen = []
        for i in range(length):
            choosen.append(sorted_by_value[i][0])

        return choosen

    def acquire_top_sentence(self):

        index = self.acquire_top_index(self.PageRank, self.percent)
        sentences = self.Sentences

        sentences_list = []
        for i in range(len(index)):
            sentences_list.append(sentences[index[i]])

        self.Top_Sentence = sentences_list
